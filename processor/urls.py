from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.ArticleListView.as_view(), name='article_list'),
    path('process/', views.ProcessorCreateView.as_view(), name='result_processor'),
    path('<int:pk>/edit/',
         views.ArticleUpdateView.as_view(), name='article_edit'),
    path('<int:pk>/',
         views.ArticleDetailView.as_view(), name='article_detail'),
    path('<int:pk>/delete/',
         views.ArticleDeleteView.as_view(), name='article_delete'),
    url('processor/process/processResult/', views.processResult),

	]

